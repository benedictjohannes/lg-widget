# Some configuration you can modify

- the whole logic lies in `./src/App.js`
- the number of highlighted players at the top with blue background can be configured at line 4 in the above file, you can change this
- you can control widget styling in `./src/style.scss`


# Building

This project is built using [Create React App](https://create-react-app.dev/), 
in short, some steps needed to build this (execute using command line):
- Make sure [NodeJS](https://nodejs.org/en/download/) is installed.
- Clone this repo, `git clone https://gitlab.com/yappy.benedict/lg-widget.git` (or, download as zip and extract)
- Change to the directory of repo, `cd lg-widget`
- Install dependencies: run `npm install`
- (only if you want to run for development): `npm start`
- compile to produce the `build` folder: `npm run build`

# Serving

- make sure all build steps are completed and the `build` folder already exist with `index.html` inside it.
- run `npm run serve`

Note:
- The build step will build the app into `build` folder, which is a static file without need for any backend engine (like PHP).
- The build folder can be deployed into any HTML serving engine (like Nginx or Apache). 
- Directly opening `build/index.html` from Windows Explorer is *not* supported.

# Player logos

Player logos are configurable at run time. 
- Add the logo images file to the `build` folder (root folder for static HTML serving)
- Reference the name of the image file (for all players, are referencing `logo.png`, as example) when editing players
- Logo will update and use the referred image.

Or, if you want to include the player logos at build time, simply copy the image files into `public` folder (everything inside will be copied into `build` folder on compilation)