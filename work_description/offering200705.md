# Timeline

Untuk kejar deadline pak Vincent bener2 hari Rabu depan ini **(15-jul-2020)**

Sy harus ada feature complete working preview *selambatnya* **13-jul-2020**

# Price

I'll help Pak Vincent for this a one-off tight-deadline job for 

**Rp. 765k**


# Deliverables

Sy akan buatkan dengan basic mirip POC yang sy share di [ligagame-poc.web.app/](https://ligagame-poc.web.app/)  
Dgn additional features berikut (english below):

## Data Input

- add, remove, modify player in players list
- checkbox for each player: eliminated VS not eliminated
- player data saved locally in the browser (survives page refresh)
- *if requested* hide widget button:
the whole widget will be animated (shrinked) to a destination corner (i.e. top right) and timing configurable  to client request
- *if requested* player logo image:  
by copass player image to a folder, when add player, input image file name

## widget display
- animation for players when the team rank order changes
- animation for elimination:  
    - Color change to indicate elimination 
    - some wait time (configurable to client request)
    - move to eliminated group (displayed at bottom)
- setting for displayed number of players:  
i.e., top 10, such that the whole players list can be more than 10
- exact pixel for widget size (to ensure proper capture by the broadcasting software)
- design customization for CSS properties borders, background colors / images, fonts, spacings
