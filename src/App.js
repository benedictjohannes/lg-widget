import React, { useState, useEffect, useRef } from 'react'
import './style.scss'

const totalPlayers = 10
const playersHeight = 40
const gutterHeight = 48

const countWidgetHeight = total => {
    let totalN = Math.min(total, totalPlayers)
    return totalN * playersHeight + gutterHeight
}

const initPlayerEditorData = { score: 0, name: 'Player', logo: 'logo.png' }

const initPlayersData = [
    { score: 10, name: 'Player1', logo: 'logo.png' },
    { score: 9, name: 'Player2', logo: 'logo.png' },
    { score: 8, name: 'Player3', logo: 'logo.png' },
    { score: 7, name: 'Player4', logo: 'logo.png' },
    { score: 6, name: 'Player5', logo: 'logo.png' },
    { score: 5, name: 'Player6', logo: 'logo.png' },
    { score: 4, name: 'Player7', logo: 'logo.png' },
    { score: 3, name: 'Player8', logo: 'logo.png' },
    { score: 2, name: 'Player9', logo: 'logo.png' },
    { score: 1, name: 'Player10', logo: 'logo.png' },
]

const getInitPlayerData = () => {
    let lsPlayerData = localStorage.getItem('playerdata')
    let parsedLsPlayerData = lsPlayerData ? JSON.parse(lsPlayerData) : null
    //return initPlayersData
    return Array.isArray(parsedLsPlayerData)
        ? parsedLsPlayerData
        : initPlayersData
}

const PlayerWidgetRow = ({
    score,
    name,
    order,
    logo,
    alivePlayers,
    killed,
}) => {
    let className = !Number.isFinite(killed)
        ? `player-${order} highlight`
        : `player-${order}`
    return (
        <div className={className}>
            <div>
                <span className='number'>{/* #{order + 1} */}KO</span>
                <img src={logo} alt={logo} />
                <div>{name}</div>
            </div>
            {/* <div>{score}</div> */}
        </div>
    )
}

const PlayerScoreEditRow = ({
    player,
    index,
    setPlayerScore,
    togglePlayerLife,
}) => (
    <div className='row align-baseline'>
        <div className='col-6 d-flex flex-row align-items-center'>
            <div style={{ height: 40 }}>
                <img
                    style={{ height: '100%' }}
                    src={player.logo}
                    alt={player.logo}
                />
            </div>
            <div>
                <p>{player.name}</p>
            </div>
        </div>
        <div className='col-6'>
            <div className='d-flex flex-row align-items-center'>
                <input
                    className='form-control'
                    type='number'
                    placeholder='Score'
                    value={player.score}
                    onChange={e => {
                        setPlayerScore(index, e.target.value)
                    }}
                />
                <span className='ml-4 mr-2'>Dead</span>
                <div className='custom-control custom-switch '>
                    <input
                        className='custom-control-input '
                        type='checkbox'
                        checked={player.killed}
                        onChange={() => {}}
                    />
                    <label
                        className='custom-control-label'
                        onClick={togglePlayerLife}
                    ></label>
                </div>
            </div>
        </div>
    </div>
)
const PlayerScoreEditor = ({
    players,
    setPlayerScore,
    alivePlayers,
    setAlivePlayers,
    togglePlayerLife,
}) => (
    <div>
        {/*
        <div className='d-flex flex-row align-items-baseline'>
            <div className='mr-3'>
                <p>Number of Alive Players</p>
            </div>
            <div className='flex-grow-1'>
                <input
                    className='form-control'
                    type='number'
                    placeholder='Alive Players'
                    value={alivePlayers}
                    onChange={e => {
                        setAlivePlayers(e.target.value)
                    }}
                />
            </div>
        </div> 
        */}
        {players.map((player, index) => (
            <PlayerScoreEditRow
                key={index}
                index={index}
                player={player}
                setPlayerScore={setPlayerScore}
                togglePlayerLife={() => togglePlayerLife(index)}
            />
        ))}
    </div>
)

const PlayerDataRowDisplay = ({ row, rm, edit, active }) => {
    let { name, logo } = row
    return (
        <div className='d-flex flex-row justify-content-between'>
            <div style={{ height: 40 }}>
                <img src={logo} style={{ height: '100%' }} alt={logo} />
                <span>{name}</span>
            </div>
            <div>
                <button
                    className='btn btn-warning mr-2'
                    onClick={rm}
                    disabled={active}
                >
                    Delete
                </button>
                <button
                    className={`btn btn-${active ? 'success' : 'primary'}`}
                    onClick={edit}
                >
                    {active ? 'Cancel' : 'Edit'}
                </button>
            </div>
        </div>
    )
}

const PlayersDataEditor = ({ players, setPlayers }) => {
    const [editedPlayer, setEditedPlayer] = useState(null)
    const [editedPlayerData, setEditedPlayerData] = useState(
        initPlayerEditorData
    )
    useEffect(() => {
        if (Number.isFinite(editedPlayer))
            setEditedPlayerData(players[editedPlayer])
    }, [editedPlayer, players])
    const delPlayer = i => {
        let newPlayers = [...players]
        newPlayers.splice(i, 1)
        setEditedPlayer(null)
        setPlayers(newPlayers)
    }
    const savePlayer = () => {
        if (Number.isFinite(editedPlayer)) {
            let newPlayers = [...players]
            newPlayers[editedPlayer] = editedPlayerData
            setPlayers(newPlayers)
            setEditedPlayer(null)
            setEditedPlayerData(initPlayerEditorData)
        } else {
            setPlayers(players => [...players, editedPlayerData])
            setEditedPlayer(null)
            setEditedPlayerData(initPlayerEditorData)
        }
    }
    return (
        <div>
            <div>
                {players.map((row, index) => (
                    <PlayerDataRowDisplay
                        row={row}
                        key={index}
                        edit={() => {
                            if (editedPlayer === index) {
                                setEditedPlayer(null)
                            } else {
                                setEditedPlayer(index)
                            }
                        }}
                        active={index === editedPlayer}
                        rm={() => {
                            delPlayer(index)
                        }}
                    />
                ))}
            </div>
            <h4 className='mt-4'>
                {Number.isFinite(editedPlayer) ? 'Edit ' : 'Add '} Player
            </h4>
            <div className='row'>
                <div className='col-6'>
                    <p className='mb-0'>Player Name</p>
                    <input
                        className='form-control'
                        placeholder='Player Name'
                        value={editedPlayerData.name}
                        onChange={e => {
                            let { value } = e.target
                            setEditedPlayerData(p => ({
                                ...p,
                                name: value,
                            }))
                        }}
                    />
                </div>
                <div className='col-6'>
                    <p className='mb-0'>Logo File</p>
                    <input
                        className='form-control'
                        placeholder='Player Name'
                        value={editedPlayerData.logo}
                        onChange={e => {
                            let { value } = e.target
                            setEditedPlayerData(p => ({
                                ...p,
                                logo: value,
                            }))
                        }}
                    />
                </div>
            </div>
            <div className='d-flex flex-row justify-content-end mt-2'>
                <button className='btn btn-primary' onClick={savePlayer}>
                    {Number.isFinite(editedPlayer) ? 'Save' : 'Add'}
                </button>
            </div>
            <div className='d-flex flex-row align-items-baseline justify-content-between mt-5'>
                <h3>Reset Player Data</h3>
                <div>
                    <button
                        className='btn btn-danger mr-2'
                        onClick={() => setPlayers(initPlayersData)}
                    >
                        Reset
                    </button>
                    <button
                        className='btn btn-danger'
                        onClick={() =>
                            setPlayers(
                                initPlayersData.map(p => ({
                                    name: p.name,
                                    score: p.score,
                                }))
                            )
                        }
                    >
                        Reset (No Logo)
                    </button>
                </div>
            </div>
        </div>
    )
}

const getAlivePlayers = t =>
    localStorage.getItem('aliveplayers')
        ? localStorage.getItem('aliveplayers')
        : t

const App = () => {
    let throttle = useRef(null)
    const [editorMode, setEditorMode] = useState(false)
    const toggleEditorMode = () => setEditorMode(!editorMode)
    const [players, setPlayers] = useState(getInitPlayerData())
    const [alivePlayers, setAlivePlayers] = useState(
        getAlivePlayers(players.length)
    )
    const [throttledPlayers, setThrottledPlayers] = useState([])
    const [displayedPlayers, setDisplayedPlayers] = useState([])

    const setPlayerScore = (i, v) => {
        setPlayers(players => {
            let newPlayer = { ...players[i], score: v }
            let newPlayers = [...players]
            newPlayers[i] = newPlayer
            return newPlayers
        })
    }
    const togglePlayerLife = i => {
        setPlayers(players => {
            let newPlayer = { ...players[i] }
            if (Number.isFinite(newPlayer.killed)) {
                let newPlayers = [...players]
                newPlayer.killed = false
                newPlayers[i] = newPlayer
                return newPlayers
            } else {
                let newPlayers = [...players]
                let maxKilledPlayersNumber = players.reduce(
                    (sum, curr) =>
                        Number.isFinite(curr.killed) ? sum + 1 : sum,
                    0
                )
                newPlayer.killed = maxKilledPlayersNumber + 1
                newPlayers[i] = newPlayer
                return newPlayers
            }
        })
    }
    useEffect(() => {
        if (throttle.current) clearTimeout(throttle.current)
        const timeout = setTimeout(() => {
            setThrottledPlayers(
                players.map((player, index) => ({ ...player, index }))
            )
            localStorage.setItem('playerdata', JSON.stringify(players))
        }, 250)
        throttle.current = timeout
    }, [players])
    useEffect(() => {
        localStorage.setItem('aliveplayers', alivePlayers)
    }, [alivePlayers])
    useEffect(() => {
        let playersWithIndex = [...throttledPlayers]
        let sortedPlayers = [...playersWithIndex].sort((a, b) => {
            if (Number.isFinite(a.killed) === Number.isFinite(b.killed)) {
                return Number.isFinite(a.killed)
                    ? b.killed - a.killed
                    : b.score - a.score
            } else {
                return a.killed ? 1 : -1
            }
        })
        let playersWithOrder = playersWithIndex
            .map(o => {
                let order = sortedPlayers.findIndex(s => s.index === o.index)
                let toReturn = { ...o, order }
                if (Number.isFinite(o.prevOrder)) {
                    toReturn.prevOrder = o.prevOrder
                } else {
                    toReturn.prevOrder = order
                }
                return toReturn
            })
            .slice(0, totalPlayers)
        setDisplayedPlayers(playersWithOrder)
    }, [throttledPlayers])
    return (
        <div className='container'>
            <div className='row'>
                <div className='col-6'>
                    <h1>Data Input</h1>

                    <div className='d-flex flex-row align-items-baseline mb-3'>
                        <div className='flex-grow-1 mr-2'>
                            <h3 className='mt-4'>
                                {editorMode ? 'Players Data' : 'Score Data'}
                            </h3>
                        </div>
                        <div>
                            <button
                                className='btn btn-primary'
                                onClick={toggleEditorMode}
                            >
                                {editorMode ? 'Edit Scores' : 'Edit Players'}
                            </button>
                        </div>
                    </div>
                    {editorMode ? (
                        <PlayersDataEditor
                            players={players}
                            setPlayers={setPlayers}
                        />
                    ) : (
                        <PlayerScoreEditor
                            players={players}
                            setPlayerScore={setPlayerScore}
                            alivePlayers={alivePlayers}
                            setAlivePlayers={setAlivePlayers}
                            togglePlayerLife={togglePlayerLife}
                        />
                    )}
                </div>
                <div className='col-6'>
                    <h1>Widget Result</h1>
                    <div
                        className='widget'
                        style={{ height: countWidgetHeight(players.length) }}
                    >
                        <div
                            style={{ height: gutterHeight }}
                            className='gutter'
                        >
                            <p>Alive</p>
                        </div>
                        {displayedPlayers.map(player => (
                            <PlayerWidgetRow
                                key={player.index}
                                {...player}
                                alivePlayers={alivePlayers}
                            />
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default App
